import { Component, OnInit } from '@angular/core';
import { ProjectIstService } from './services/project-ist.service';
import { Project } from './models/project-model';
import { Http, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  isLoading = true;
  projects: Array<Project>;

  constructor(private _projectListService: ProjectIstService,
  private _http: Http) { }

  ngOnInit() {

      this.getAllProjects();
  }

  getAllProjects() {
      this._projectListService.getAll().subscribe(   // Se reciben los proyectos que vienen en la respuesta
          (data: Project[]) => {
              // Next
              this.projects = data;
              this.isLoading = false;
          },
          err => {
              console.error(err);
          },
          () => {
              console.log('Finished');
          }
      );
  }

  onDeleteProject(project: Project) {

    this._projectListService.deleteProject(project).subscribe((data) => {
          console.log(data);
          this.getAllProjects();
      });

      // Se creó el método para eliminar en project-ist.service.ts, se instancia el ProjectListService para que lea el método delete de ese archivo
  }

  setData(sortedProjects) {
      this.projects = sortedProjects;
  }
}
