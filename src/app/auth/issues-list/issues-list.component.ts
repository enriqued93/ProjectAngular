import { Component, OnInit } from '@angular/core';
import { IssuesListService } from './services/issues-list.service';
import { Issue } from './models/issues-model';

import { Http } from '@angular/http';


@Component({
  selector: 'app-issues-list',
  templateUrl: './issues-list.component.html',
  styleUrls: ['./issues-list.component.css']
})
export class IssuesListComponent implements OnInit {

  isLoading = true;
   issues: Array<Issue> = [];

  constructor(private _issueListServices: IssuesListService,
              private _http: Http) { }

  ngOnInit() {

      this.getAllIssues();
  }

  getAllIssues() {
    this._issueListServices.getAll().subscribe(
        (data: Issue[]) => {
          this.issues = data;
          this.isLoading = false;
        },
        err => {
          console.error(err);
        },
        () => {
          console.log('Finished Bro');
      }
    );
  }

}
