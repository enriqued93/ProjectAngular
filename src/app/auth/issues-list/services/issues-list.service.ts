import { Injectable } from '@angular/core';
import { Issue } from '../models/issues-model';
import {AuthenticationService} from '../../../common/services/authentication.service';


import { Http } from '@angular/http';
import { HttpService } from '../../../common/services/http.service';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';

@Injectable()
export class IssuesListService extends HttpService{

  constructor(public _http: Http, public _authService: AuthenticationService) {

    super(_http);
  }


  getAll(): Observable<Array<Issue>> {

    const url = `${this.apiBaseURL}/issues`;
    const token = this._authService.user.api_token;

    return this.get(url, token);


  }
}


