import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProjectListComponent } from './auth/project-list/project-list.component';

import { LoaderComponent } from './common/loader/loader.component';
import { HeaderComponent } from './common/header/header.component';
import {ProjectIstService} from './auth/project-list/services/project-ist.service';
import { LoginComponent } from './public/login/login.component';
import { routes } from './routes';  // Importo el archivo de rutas para que Angular las lea

import { Ng2Webstorage } from 'ngx-webstorage';
import { HomeComponent } from './auth/home/home.component';
import { RouterModule } from '@angular/router';
import { AuthenticationService } from './common/services/authentication.service';
import { HttpService } from './common/services/http.service';
import { AuthGuard } from './common/guards/auth.guard';
import { PublicGuard } from './common/guards/public.guard';
import { NotFoundComponent } from './common/not-found/not-found.component';
import { SortingComponent } from './common/sorting/sorting.component';




@NgModule({
  declarations: [
    AppComponent,
    ProjectListComponent,
    LoaderComponent,
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    NotFoundComponent,
    SortingComponent,
  ],
  imports: [
    BrowserModule,
      HttpModule,
      FormsModule,  // Se importa para trabajar con el formulario y NgModel
      ReactiveFormsModule,
      RouterModule.forRoot(routes), // Declaro el archivo de routes como principal
      Ng2Webstorage

  ],
  providers: [ProjectIstService, AuthenticationService,
      HttpService, AuthGuard, PublicGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
