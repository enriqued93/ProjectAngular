import { Routes } from '@angular/router';

import { LoginComponent } from './public/login/login.component';
import { HomeComponent }  from './auth/home/home.component';
import { ProjectListComponent } from './auth/project-list/project-list.component';
import {NotFoundComponent} from './common/not-found/not-found.component';
import {PublicGuard} from './common/guards/public.guard';
import {AuthGuard} from './common/guards/auth.guard';

// Cuando visiten la raíz redirige al Login
// Carga el LoginComponent
// Nos redirige a Home
// Cuando visiten Poryectos muestra el componente de Proyectos

export const routes: Routes = [
    {
        path: '', pathMatch : 'full', redirectTo : '/login'

    },

    {
        path: 'login', component: LoginComponent, pathMatch: 'full', canActivate: [PublicGuard]
    },

    {
        path: 'home', component: HomeComponent, canActivate: [AuthGuard]
    },
    {
        path: 'proyectos', component: ProjectListComponent, canActivate: [AuthGuard]
    },
    {
        path: '**', component: NotFoundComponent
    }
];